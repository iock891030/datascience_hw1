from typing import cast
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import csv
import operator
from mlxtend.plotting import scatterplotmatrix
from sklearn import metrics
from sklearn.base import ClassifierMixin
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.utils.multiclass import type_of_target

trainFile_path = "./train.csv"
testFile_path = "./test.csv"
ex_submit_path = "./ex_submit.csv"
submit_path = "./submit.csv"

trainFile = pd.read_csv(trainFile_path)
outputTestfile = pd.read_csv(testFile_path)
trainFile = trainFile.dropna(how="any")
# print(trainFile.head())


def preprofile(file, mode):
    if mode == 'Data':
        Attribute = 'Attribute22'
    elif mode == "Target":
        Attribute = 'Attribute23'

    for i in file[Attribute]:
        if i == "No":
            i = 0.0
        elif i == "Yes":
            i = 1.0


trainColumns = ['Attribute3', 'Attribute4', 'Attribute5',
                'Attribute6', 'Attribute7', 'Attribute9',
                'Attribute12', 'Attribute13', 'Attribute14',
                'Attribute15', 'Attribute16', 'Attribute17', 'Attribute18',
                'Attribute19', 'Attribute20', 'Attribute21', 'Attribute22']

_x = pd.DataFrame(trainFile, columns=trainColumns)
_y = pd.DataFrame(trainFile, columns=["Attribute23"])
_test = pd.DataFrame(outputTestfile, columns=trainColumns)
test = _test.fillna(0)
x = _x.fillna(0)
y = _y.fillna(0)
preprofile(x, 'Data')
preprofile(y, "Target")
y = y.astype('bool')
x["Attribute22"] = x["Attribute22"].astype('bool')
test["Attribute22"] = test["Attribute22"].astype('bool')


x_train, x_test, y_train, y_test = train_test_split(
    x, y, test_size=0.3, random_state=0)
print(x_train.shape)
print(x_test.shape)


forest = RandomForestClassifier(
    criterion="entropy", n_estimators=100, random_state=0, n_jobs=-1)
forest.fit(x_train.values, y_train.values)
y_predict = forest.predict(x_test)
test_predict = forest.predict(test)
test_output = test_predict.astype("int")


_id = [i for i in range(test_predict.size)]
id = np.array(_id, dtype=np.float)
id = id.astype("str")

dic = {
    'id': id,
    'ans': test_output
}
df = pd.DataFrame(dic)
df.to_csv('submit.csv', index=False)

submit = pd.read_csv(submit_path)

rate = forest.score(x_test, y_test)
print(type_of_target(test_predict))
print("acc = %.3f" % rate)
